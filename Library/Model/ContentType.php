<?php
namespace Airhead\Library\Model;

class ContentType
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var object
     */
    private $fields;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = (string)$code;
    }

    /**
     * @return object
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param object $fields
     */
    public function setFields($fields)
    {
        $this->fields = (object)$fields;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string)$name;
    }
}