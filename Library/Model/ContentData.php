<?php
namespace Airhead\Library\Model;

class ContentData
{
    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $dataBig;

    /**
     * @var int
     */
    private $dataNumeric;

    /**
     * @var string
     */
    private $field;

    /**
     * @var int
     */
    private $id;

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = (string)$data;
    }

    /**
     * @return string
     */
    public function getDataBig()
    {
        return $this->dataBig;
    }

    /**
     * @param string $dataBig
     */
    public function setDataBig($dataBig)
    {
        $this->dataBig = (string)$dataBig;
    }

    /**
     * @return int
     */
    public function getDataNumeric()
    {
        return $this->dataNumeric;
    }

    /**
     * @param int $dataNumeric
     */
    public function setDataNumeric($dataNumeric)
    {
        $this->dataNumeric = (int)$dataNumeric;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = (string)$field;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }
}