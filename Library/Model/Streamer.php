<?php
namespace Airhead\Library\Model;

class Streamer
{
    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $game;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $status;

    /**
     * @param int $id
     * @param string $name
     * @param string $link
     * @param string $game
     * @param string $avatar
     * @param string $status
     */
    public function __construct($id, $name, $link, $game, $avatar, $status)
    {
        $this->id = (int)$id;
        $this->name = (string)$name;
        $this->link = (string)$link;
        $this->game = (string)$game;
        $this->avatar = (string)$avatar;
        $this->status = (string)$status;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @return string
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}