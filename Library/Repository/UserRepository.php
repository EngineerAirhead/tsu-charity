<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\User;

class UserRepository extends BaseRepository
{
    /**
     * @param string[] $inputData
     * @return int|bool
     */
    public function create($inputData)
    {
        $this->db->startTransaction();

        $userId = $this->db->insert('user', [
            'id_user_role' => $inputData['id_user_role'],
            'username' => $inputData['username'],
            'name' => $inputData['name'],
            'email' => $inputData['email'],
            'password' => $inputData['password'],
            'hash' => $inputData['hash'],
            'reset_password' => 1
        ]);

        if($userId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $userId;
    }

    /**
     * @param User $user
     * @param string[] $inputData
     * @return bool|User
     */
    public function update(User $user, $inputData)
    {
        $this->db->startTransaction();

        $userData = [
            'username' => $inputData['username'],
            'name' => $inputData['name'],
            'email' => $inputData['email'],
            'id_user_role' => (int)$inputData['id_user_role'],
            'reset_password' => (isset($inputData['reset_password']) === true) ? 1 : null,
        ];

        // Update user
        $this->db->where('id', $user->getId());
        $update1 = $this->db->update('user', $userData, 1);
        if ($update1 === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $this->findById($user->getId());
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user)
    {
        $this->db->startTransaction();

        // Remove content
        $this->db->where('id', $user->getId());
        $delete = $this->db->delete('user', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return true;
    }

    /**
     * @param User $user
     * @param string[] $inputData
     * @return bool|User
     */
    public function updatePassword(User $user, $inputData)
    {
        $this->db->startTransaction();

        $userData = [
            'password' => $inputData['password'],
            'hash' => $inputData['hash'],
            'reset_password' => null,
        ];

        // Update user
        $this->db->where('id', $user->getId());
        $update1 = $this->db->update('user', $userData, 1);
        if ($update1 === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $this->findById($user->getId());
    }

    /**
     * @return User[]
     */
    public function findAll()
    {
        $users = $this->db->get('user');
        if (count($users) > 0) {
            foreach ($users as $key => $user) {
                $users[$key] = $this->buildUser($user);
            }
        }

        return $users;
    }

    /**
     * @param string $value
     * @return User|bool
     */
    public function findById($value)
    {
        $this->db->where('id', (int)$value);
        $user = $this->db->getOne('user');

        if ($user === null) {
            return false;
        }

        return $this->buildUser($user);
    }

    /**
     * @param string $value
     * @return User|bool
     */
    public function findByEmail($value)
    {
        $this->db->where('email', (string)$value);
        $user = $this->db->getOne('user');

        if ($user === null) {
            return false;
        }

        return $this->buildUser($user);
    }

    /**
     * @param mixed[] $userData
     * @return User
     */
    private function buildUser($userData)
    {
        $user = new User();
        $user->setId($userData['id']);
        $user->setUsername($userData['username']);
        $user->setName($userData['name']);
        $user->setEmail($userData['email']);
        $user->setPassword($userData['password']);
        $user->setHash($userData['hash']);
        $user->setIsRoot($userData['is_root'] === 1 ? true : false);
        $user->setResetPassword($userData['reset_password'] === 1 ? true : false);
        $user->setRole((new RoleRepository($this->db))->findById($userData['id_user_role']));

        return $user;
    }
}