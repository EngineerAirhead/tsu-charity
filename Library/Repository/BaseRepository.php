<?php
namespace Airhead\Library\Repository;

class BaseRepository
{
    /**
     * @var \MysqliDb
     */
    public $db;

    /**
     * @param \MysqliDb $db
     */
    public function __construct(\MysqliDb $db)
    {
        $this->db = $db;
    }
}