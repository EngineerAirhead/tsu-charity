<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\File;

class FileRepository extends BaseRepository
{
    /**
     * @param string[] $inputData
     * @param int $contentId
     * @return int|bool
     */
    public function create($inputData, $contentId)
    {
        $this->db->startTransaction();

        $fileId = $this->db->insert('file', [
            'id_content' => $contentId,
            'key' => $inputData['key'],
            'filetype' => $inputData['filetype'],
            'name' => $inputData['name'],
            'dir' => $inputData['dir'],
        ]);

        if($fileId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $fileId;
    }

    /**
     * @param File $file
     * @return bool
     */
    public function delete(File $file)
    {
        $this->db->startTransaction();

        // Remove content information
        $this->db->where('id', $file->getId());
        $delete = $this->db->delete('file', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return true;
    }

    /**
     * @param $contentId
     * @return File[]|bool
     */
    public function findByContentId($contentId)
    {
        $this->db->where('id_content', $contentId);
        $files = $this->db->get('file');

        // Check for response
        if (is_null($files) === false) {
            $return = [];
            foreach ($files as $file) {
                $return[] = $this->buildFile($file);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param int $contentId
     * @param string $key
     * @return File|bool
     */
    public function findFileByContentIdAndKey($contentId, $key)
    {
        $this->db->where('id_content', $contentId);
        $this->db->where('`key`', $key);
        $fileData = $this->db->getOne('file');

        // Check for response
        if (is_null($fileData) === false) {
            return $this->buildFile($fileData);
        }

        return false;
    }

    /**
     * @param mixed[] $data
     * @return File
     */
    private function buildFile($data)
    {
        $file = new File();

        $file->setId($data['id']);
        $file->setContentId($data['id_content']);
        $file->setKey($data['key']);
        $file->setFileType($data['filetype']);
        $file->setName($data['name']);
        $file->setDir($data['dir']);

        return $file;
    }
}