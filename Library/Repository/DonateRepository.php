<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\Donation;

class DonateRepository extends BaseRepository
{
    /**
     * @param string $orderType
     * @return Donation[]|bool
     */
    public function findAll($orderType = 'DESC')
    {
        $this->db->orderBy('date_create', $orderType);
        $donations = $this->db->get('donation');

        $return = [];

        if (is_null($donations) === true) {
            return $return;
        }

        foreach ($donations as $donation) {
            $return[] = $this->buildDonation($donation);
        }

        return $return;
    }

    /**
     * @return array
     */
    public function findAllConfirmed()
    {
        $this->db->where('status', 'confirmed');
        $donations = $this->db->get('donation');

        $return = [];

        if (is_null($donations) === true) {
            return $return;
        }

        foreach ($donations as $donation) {
            $return[] = $this->buildDonation($donation);
        }

        return $return;
    }

    /**
     * @return mixed
     */
    public function findSumCreatedPerHour()
    {
        return $this->db->rawQuery('
            SELECT FROM_UNIXTIME(FLOOR(UNIX_TIMESTAMP(date_create)/3600)*3600) AS date
                 , SUM(amount) as amount
            FROM donation
            GROUP
                BY date
        ');
    }

    /**
     * @return mixed
     */
    public function findSumConfirmedPerHour()
    {
        return $this->db->rawQuery('
            SELECT FROM_UNIXTIME(FLOOR(UNIX_TIMESTAMP(date_update)/3600)*3600) AS date
                 , SUM(amount) as amount
            FROM donation
            WHERE
                status = "confirmed"
            GROUP
                BY date;
        ');
    }

    /**
     * @return Donation|bool
     */
    public function findById($id)
    {
        $this->db->where('id', $id);
        $donation = $this->db->getOne('donation');

        if (is_null($donation) === true) {
            return false;
        }

        return $this->buildDonation($donation);
    }

    /**
     * @return int
     */
    public function getTotalDonationAmount()
    {
        return (int)$this->db->rawQueryValue('SELECT IFNULL(SUM(`amount`), 0) FROM `donation` where `status` = "confirmed" LIMIT 1');
    }

    /**
     * @param $limit
     * @return Donation[]|bool
     */
    public function getTopDonators($limit)
    {
        $donations =  $this->db->rawQuery('
            SELECT
              id,
              SUM(amount) as amount,
              name,
              email,
              date_create,
              date_update,
              status,
              transaction_id
            FROM donation
            WHERE
                status = "confirmed"
            GROUP
                BY email    
            ORDER BY amount DESC
            LIMIT ' . $limit
        );

        // Check for response
        if (is_null($donations) === false) {
            $return = [];
            foreach ($donations as $donation) {
                $return[] = $this->buildDonation($donation);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param string $transactionId
     * @return Donation|bool
     */
    public function getDonationByTransactionId($transactionId)
    {
        $this->db->where('transaction_id', $transactionId);
        $result = $this->db->getOne('donation');

        if ($result === null) {
            return false;
        }

        return $this->buildDonation($result);
    }

    /**
     * @param mixed[] $inputData
     * @return bool|Donation
     */
    public function create($inputData)
    {
        $this->db->startTransaction();

        $donationId = $this->db->insert('donation', [
            'transaction_id' => $inputData['transaction_id'],
            'name' => $inputData['name'],
            'email' => $inputData['email'],
            'amount' => $inputData['amount'],
            'status' => $inputData['status'],
            'date_create' => $inputData['date_create'],
            'date_update' => (isset($inputData['date_update']) === true) ? $inputData['date_update'] : null
        ]);

        if($donationId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $this->buildDonation(array_merge($inputData, [
            'id' => $donationId,
            'date_update' => (isset($inputData['date_update']) === true) ? $inputData['date_update'] : null
        ]));
    }

    /**
     * @param Donation $donation
     * @return bool
     */
    public function delete(Donation $donation)
    {
        $this->db->startTransaction();

        // Remove content
        $this->db->where('id', $donation->getId());
        $delete = $this->db->delete('donation', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return true;
    }

    /**
     * @param string $transactionId
     * @return bool
     */
    public function confirmTransaction($transactionId)
    {
        $this->db->where('transaction_id', $transactionId);

        return $this->db->update('donation', [
            'status' => 'confirmed',
            'date_update' => date('Y-m-d H:i:s')
        ], 1);
    }

    /**
     * @param mixed[] $data
     * @return Donation
     */
    private function buildDonation($data)
    {
        $donation = new Donation();

        $donation->setId($data['id']);
        $donation->setAmount($data['amount']);
        $donation->setName($data['name']);
        $donation->setEmail($data['email']);
        $donation->setDateCreate($data['date_create']);
        if ($data['date_update'] !== null) {
            $donation->setDateUpdate($data['date_update']);
        }
        $donation->setStatus($data['status']);
        $donation->setTransactionId($data['transaction_id']);

        return $donation;
    }
}