<?php
namespace Airhead\Library\Repository;

use Airhead\Library\Model\ContentType;

class ContentTypeRepository extends BaseRepository
{
    /**
     * @param mixed[] $inputData
     * @return int
     */
    public function create($inputData)
    {
        $this->db->startTransaction();

        $contentTypeId = $this->db->insert('content_type', [
            'name' => $inputData['name'],
            'code' => $inputData['code'],
            'fields' => serialize(json_encode([
                'default' => (isset($inputData['default']) === true) ? $inputData['default'] : [],
                'extra' => (isset($inputData['extra']) === true) ? array_values($inputData['extra']) : [],
            ]))
        ]);

        if($contentTypeId === false) {
            $this->db->rollback();
            return false;
        }

        $this->db->commit();

        return $contentTypeId;
    }
    /**
     * @param ContentType $contentType
     * @param string[] $inputData
     * @return bool
     */
    public function update(ContentType $contentType, $inputData)
    {
        $this->db->startTransaction();

        $tableData = [
            'name' => $inputData['name'],
            'code' => $inputData['code'],
            'fields' => serialize(json_encode([
                'default' => (isset($inputData['default']) === true) ? $inputData['default'] : [],
                'extra' => (isset($inputData['extra']) === true) ? array_values($inputData['extra']) : [],
            ]))
        ];

        $this->db->where('id', $contentType->getId());
        $update = $this->db->update('content_type', $tableData, 1);
        if ($update === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @param ContentType $contentType
     * @return bool
     */
    public function delete(ContentType $contentType)
    {
        $this->db->startTransaction();

        // Remove content
        $this->db->where('id', $contentType->getId());
        $delete = $this->db->delete('content_type', 1);
        if ($delete === false) {
            $this->db->rollback();
            return false;
        }

        return $this->db->commit();
    }

    /**
     * @return ContentType[]|bool
     */
    public function findAll()
    {
        $this->db->orderBy('position', 'ASC');
        $data = $this->db->get('content_type');

        // Check for response
        if (is_null($data) === false) {
            $return = [];
            foreach ($data as $contentType) {
                $return[] = $this->buildContentType($contentType);
            }

            return $return;
        }

        return false;
    }

    /**
     * @param int $id
     * @return ContentType|bool
     */
    public function findById($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('content_type');

        if (is_null($data) === true) {
            return false;
        }

        return $this->buildContentType($data);
    }

    /**
     * @param string $code
     * @return ContentType|bool
     */
    public function findByCode($code)
    {
        $this->db->where('code', $code);
        $data = $this->db->getOne('content_type');

        if (is_null($data) === true) {
            return false;
        }

        return $this->buildContentType($data);
    }

    /**
     * @param array $data
     * @return ContentType
     */
    private function buildContentType($data)
    {
        $contentType = new ContentType();
        $contentType->setId($data['id']);
        $contentType->setName($data['name']);
        $contentType->setCode($data['code']);
        $contentType->setFields(json_decode(unserialize($data['fields'])));

        return $contentType;
    }
}