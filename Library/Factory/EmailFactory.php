<?php
namespace Airhead\Library\Factory;

use Airhead\Cosmo\View\DonationEmailView;
use Airhead\Library\Model\Content;
use Airhead\Library\Model\ContentData;
use Airhead\Library\Model\Donation;
use Airhead\Library\Model\File;
use Airhead\Library\Repository\ContentRepository;

class EmailFactory
{
    /**
     * @var ContentRepository
     */
    private $contentRepository;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @param ContentRepository $contentRepository
     * @param string $baseUrl
     */
    public function __construct(ContentRepository $contentRepository, $baseUrl)
    {
        $this->contentRepository = $contentRepository;
        $this->baseUrl = (string)$baseUrl;
    }

    /**
     * @param Donation $donation
     * @return bool
     */
    public function sendDonationConfirmedEmail(Donation $donation)
    {
        // Send Email
        $mail = new \PHPMailer();

        $mail->setFrom('gamingmarathon@toysoldiersunite.com', 'The Gaming Mammoth');
        $mail->addAddress($donation->getEmail(), $donation->getName());
        $mail->addBCC('gamingmarathon@toysoldiersunite.com', 'The Gaming Mammoth');

        $mail->isHTML(true);

        // Build sponsor string
        $sponsorString = '';$sponsors = $this->contentRepository->findContentByType('sponsor');
        if ($sponsors !== false && count($sponsors) > 0) {
            $sponsorString .= '<table cellpadding="20" cellspacing="0" border="0" align="center" class="container" style="background: #151515; width: 600px;border-bottom:1px solid #ffcc00;">';

            $sponsorChunks = array_chunk($sponsors, 3);
            foreach ($sponsorChunks as $chunk) {
                $sponsorString .= '<tr>';

                /** @var Content $sponsor */
                foreach ($chunk as $sponsor) {

                    /** @var ContentData $link */
                    $link = $sponsor->getContentData()['link'];

                    $sponsorString .= '<td style="width:33%;text-align:center;">';
                    $sponsorString .= '<a href="'.$link->getData().'" style="color:#ffcc00;" title="'.$sponsor->getContentInformation()->getTitle().'">';

                    $files = $sponsor->getContentFiles();
                    if (isset($files['media']) === true) {
                        /** @var File $image */
                        $image = $files['media'];
                        $sponsorString .= '<img src="'.$this->baseUrl.$image->getDir().$image->getName().'" style="width:100%;max-height:100%;">';
                    } else {
                        $sponsorString .= $sponsor->getContentInformation()->getTitle();
                    }

                    $sponsorString .= '</a>';
                    $sponsorString .= '</td>';
                }

                $sponsorString .= '</tr>';
            }

            $sponsorString .= '</table>';
        }

        $mail->Subject = 'Thank you for your donation';
        $mail->Body = (new DonationEmailView($donation->getName(), $sponsorString, $this->baseUrl))->parse();

        return $mail->send();
    }
}