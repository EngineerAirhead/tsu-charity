<?php
namespace Airhead\Library\Framework;

use Slim\Http\UploadedFile;

class FileHelper
{
    /**
     * @param string $dir
     * @param int $amount
     * @return string[]
     */
    public static function randomFiles($dir, $amount = 5)
    {
        $files = glob($dir . '/*.*');
        if ($amount == 'all') {
            shuffle($files);
            $returnFiles = $files;
        } else {
            $randKeys = array_rand($files, $amount);

            $returnFiles = array();

            if (count($randKeys) > 0) {
                foreach ($randKeys as $randKey) {
                    $returnFiles[] = $files[$randKey];
                }
            }
        }

        return $returnFiles;
    }

    /**
     * @param UploadedFile $tempFile
     * @param string $destination
     * @param string $fileName
     * @return bool
     */
    public static function uploadFile(UploadedFile $tempFile, $destination, $fileName)
    {
        if (file_exists($destination) === false) {
            mkdir($destination, 0755, true);
        }

        $tempFile->moveTo($destination.$fileName);
    }

    /**
     * @param string $file
     * @return bool
     */
    public static function deleteFile($file)
    {
        if (file_exists($file) === false) {
            return false;
        }

        return unlink($file);
    }

    /**
     * @param string $path
     * @return bool
     */
    public static function removeEmptySubFolders($path)
    {
        $empty = true;

        foreach (glob($path.DIRECTORY_SEPARATOR."*") as $file) {
            $empty &= is_dir($file) && self::removeEmptySubFolders($file);
        }

        return $empty && rmdir($path);
    }

    /**
     * @param int $code
     * @return string
     */
    public static function getFileUploadErrorMsg($code)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The uploaded file is too big.";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }

    /**
     * @param string $file
     * @return bool
     */
    public static function isValidImageFile($file)
    {
        return in_array(self::getImageType($file), [
                IMAGETYPE_GIF,
                IMAGETYPE_JPEG,
                IMAGETYPE_PNG
            ]) === true;
    }

    /**
     * @param string $image
     * @return int
     */
    public static function getImageType($image)
    {
        if (function_exists('exif_imagetype')) {
            return exif_imagetype($image);
        }

        $r = getimagesize($image);
        return $r[2];
    }
}