<?php
namespace Airhead\Library\Framework;

use \Composer\Script\Event;

class ComposerHelper
{
    private static $removeFiles = [
        '.editorconfig',
        '.gitattributes',
        '.gitignore',
        '.hhconfig',
        '.php_cs',
        '.releasinator.rb',
        '.scrutinizer.yml',
        '.travis.yml',
        'changelog',
        'changelog.md',
        'contributing.md',
        'couscous.yml',
        'gemfile',
        'gemfile.lock',
        'generate-api.sh',
        'license',
        'license.md',
        'phpunit.integration.xml',
        'phpunit.xml',
        'phpunit.xml.dist',
        'rakefile',
        'readme.md',
        'readme.rst',
        'version',
    ];

    private static $removeDirectories = [
        '.git',
        '.github',
        'doc',
        'docs',
        'example',
        'examples',
        'sample',
        'test',
        'tests',
    ];

    /**
     * Cleanup all unwanted files within the composer vendor file
     * - docs
     * - tests
     * - etc
     */
    public static function cleanup(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');

        self::searchFolders($vendorDir);
    }

    /**
     * @param $dir
     * @return void
     */
    private static function searchFolders($dir)
    {
        $dh = scandir($dir);

        foreach ($dh as $folder) {
            if ($folder === '.' || $folder === '..') {
                continue;
            }

            if (is_dir($dir.DIRECTORY_SEPARATOR.$folder) === false) {
                if (in_array(strtolower($folder), self::$removeFiles) === true) {
                    self::removeFile($dir.DIRECTORY_SEPARATOR.$folder);
                }
                continue;
            }

            if (in_array(strtolower($folder), self::$removeDirectories) === true) {
                self::recurseRmDir($dir.DIRECTORY_SEPARATOR.$folder);
            } else {
                self::searchFolders($dir.DIRECTORY_SEPARATOR.$folder);
            }
        }
    }

    /**
     * @param $dir
     * @return bool
     */
    private static function recurseRmDir($dir)
    {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            $location = $dir.DIRECTORY_SEPARATOR.$file;
            if (is_dir($location)) {
                self::recurseRmDir($location);
            } else {
                self::removeFile($location);
            }
        }
        return rmdir($dir);
    }

    /**
     * @param $file
     */
    private static function removeFile($file)
    {
        chmod($file, 0777);
        unlink($file);
    }
}