<?php
namespace Airhead\Library\Framework;

use Symfony\Component\Yaml\Yaml;

class Config
{
    /**
     * @var array
     */
    private $config;

    /**
     * Config constructor.
     * @param $configFile
     * @throws \Exception
     */
    public function __construct($configFile)
    {
        $this->config = Yaml::parse(file_get_contents($configFile));
    }

    /**
     * @param $key
     * @return mixed
     * @throws \Exception
     */
    public function get($key)
    {
        if ($this->has($key) === false) {
            throw new \Exception("Unable to get config with key: ".$key);
        }

        return $this->config[$key];
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key)
    {
        return isset($this->config[$key]);
    }

    /**
     * @param mixed $key
     * @param mixed $value
     */
    public function set($key, $value)
    {
        $this->config[$key] = $value;
    }
}
