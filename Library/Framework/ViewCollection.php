<?php
namespace Airhead\Library\Framework;

class ViewCollection extends View
{
    /**
     * @var View[]
     */
    private $collection = [];

    /**
     * @param View[] $collection
     */
    public function __construct($collection = [])
    {
        parent::__construct();

        $this->collection = $collection;
    }

    /**
     * @param View $view
     */
    public function addSubView(View $view)
    {
        $this->collection[] = $view;
    }

    /**
     * @return View[]
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @return string
     */
    public function parse()
    {
        $return = '';;

        foreach ($this->collection as $view) {
            $return .= $view->parse();
        }

        return $return;
    }
}