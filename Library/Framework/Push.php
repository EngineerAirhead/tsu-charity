<?php
namespace Airhead\Library\Framework;

use Pushbullet\Connection;

class Push
{
    /**
     * Endpoints
     */
    const URL_EPHEMERALS = 'https://api.pushbullet.com/v2/ephemerals';

    /**
     * @var string
     */
    private $token;

    /**
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = (string)$token;
    }

    /**
     * @param array $data
     * @return object
     */
    public function pushData(array $data)
    {
        return Connection::sendCurlRequest(Connection::URL_EPHEMERALS, 'POST', [
            'type' => 'push',
            'push' => $data
        ], true, $this->token);
    }
}