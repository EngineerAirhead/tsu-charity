<?php
namespace Airhead\Library\Framework;

use PHPThumb\GD;

class Image
{
    /**
     * @var string
     */
    private $file;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var GD
     */
    private $image;

    /**
     * @param string $file
     */
    public function __construct($file)
    {
        $this->file = (string)$file;
        $this->fileName = basename($this->file);
        $this->image = new GD($this->file);
    }

    /**
     * @return array
     */
    public function getDimensions()
    {
        return $this->image->getCurrentDimensions();
    }

    /**
     * @param int $width
     * @param int $height
     * @param bool $adaptive
     * @return string
     */
    public function getImagePath($width = 0, $height = 0, $adaptive = true)
    {
        $newSize = $this->getNewWidthHeight($width, $height);
        $directory = str_replace($this->fileName, '', $this->file) . $newSize['width'] . 'x' . $newSize['height'];

        if (is_dir($directory) === false) {
            mkdir($directory);
        }

        $imagePath = $directory . DIRECTORY_SEPARATOR . $this->fileName;

        if (file_exists($imagePath) === false) {
            $image = clone $this->image;
            if ($adaptive === true) {
                $image->adaptiveResize($newSize['width'], $newSize['height']);
            } else {
                $image->resize($newSize['width'], $newSize['height']);
            }

            $image->save($imagePath);
        }

        return $imagePath;
    }

    /**
     * @param int $width
     * @param int $height
     * @return array
     */
    private function getNewWidthHeight($width, $height)
    {
        $width = ($width <= 0) ? $this->image->getCurrentDimensions()['width'] : $width;
        $height = ($height <= 0) ? $this->image->getCurrentDimensions()['height'] : $height;

        return ['width' => $width, 'height' => $height];
    }
}