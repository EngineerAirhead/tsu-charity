<?php
namespace Airhead\Library\Framework;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\App;
use Symfony\Component\Yaml\Yaml;

class Route
{
    /**
     * @param App $app
     * @param string $routeFile
     * @throws \Exception
     */
    public function __construct(App $app, $routeFile)
    {
        $routes = Yaml::parse(file_get_contents($routeFile));

        foreach ($routes as $path => $route) {
            $controller = $route['controller'];

            if ($route['type'] === 'map' && isset($route['map']) === true) {
                $app->{$route['type']}(
                    $route['map'],
                    $path,
                    function (Request $request, Response $response, $args) use ($route, $controller) {
                        return (new $controller($request, $response, $args))->{$route['method']}();
                    }
                )->setName($route['name']);
            } else {
                $app->{$route['type']}(
                    $path,
                    function (Request $request, Response $response, $args) use ($route, $controller) {
                        return (new $controller($request, $response, $args))->{$route['method']}();
                    }
                )->setName($route['name']);
            }
        }
    }
}