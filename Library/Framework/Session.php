<?php
namespace Airhead\Library\Framework;

/**
 * Our very own session class.
 */
class Session
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var bool
     */
    private $status;

    /**
     * @param string $session_token
     */
    public function __construct($session_token = 'token')
    {
        // Set the session token
        $this->token = (string)$session_token;

        // start the session
        $this->status = $this->start();
    }

    /**
     * Starts new or resumes existing session
     *
     * @access  public
     * @return  bool
     */

    public function start()
    {
        return session_start();
    }

    /**
     * End existing session, destroy, unset and delete session cookie
     *
     * @access  public
     * @return  void
     */

    public function end()
    {
        // Check is we have an active session, or set it if not!
        if($this->status !== true) {
            $this->start();
        }

        // unset session and cookie values!
        session_destroy();
        session_unset();
        setcookie(session_name(), null, 0, "/");
    }

    /**
     * @param   mixed
     * @param   mixed
     * @return  mixed
     */
    public function set($key, $value)
    {
        // Set the session value and return it.
        return $_SESSION[$this->token][$key] = $value;
    }

    /**
     * @param $key
     */
    public function remove($key)
    {
        // Check if the session value exists and unset it.
        if($this->has($key) === true) {
            unset($_SESSION[$this->token][$key]);
        }
    }

    /**
     * @param   mixed  - session key
     * @return  bool
     */
    public function has($key)
    {
        // Check if the session value exists
        return isset($_SESSION[$this->token][$key]);
    }

    /**
     * @param   mixed
     * @return  mixed
     */
    public function get($key)
    {
        // Check if the value exists
        if($this->has($key) === false) {
            return false;
        }

        // Return the session value
        return $_SESSION[$this->token][$key];
    }
}