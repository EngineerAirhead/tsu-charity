<?php
namespace Airhead\Library\Framework;

class Random
{
    /**
     * @param int $length
     * @return string
     */
    public static function createPasswordHash($length = 32)
    {
        $hash = '';
        for ($i = 0; $i < $length; $i++) {
            $hash .= chr(mt_rand(33, 126));
        }
        return $hash;
    }
}