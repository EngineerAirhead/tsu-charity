<?php
namespace Airhead\Wanda\Factory;

use Airhead\Library\Framework\Container;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Wanda\View\DashboardContentViewInterface;
use Airhead\Wanda\View\DashboardView;
use Airhead\Wanda\View\Navigation\LeftNavView;
use Airhead\Wanda\View\Navigation\TopNavView;

class DashboardViewFactory
{
    /**
     * @var ContentTypeRepository
     */
    private $contentTypeRepository;

    /**
     * @param ContentTypeRepository $contentTypeRepository
     */
    public function __construct(ContentTypeRepository $contentTypeRepository)
    {
        $this->contentTypeRepository = $contentTypeRepository;
    }

    /**
     * @param DashboardContentViewInterface $contentView
     * @return DashboardView
     */
    public function buildDashboardView(DashboardContentViewInterface $contentView)
    {
        $contentTypes = $this->contentTypeRepository->findAll();

        return new DashboardView(
            new TopNavView(Container::getSession()->get('admin')),
            new LeftNavView(
                Container::getRouter()->pathFor('index'),
                Container::getRouter()->pathFor('user'),
                ($contentTypes !== false) ? $contentTypes : []
            ),
            $contentView,
            Container::getFlash()->getMessage('notification')
        );
    }
}