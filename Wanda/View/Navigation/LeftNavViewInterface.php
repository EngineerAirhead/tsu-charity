<?php
namespace Airhead\Wanda\View\Navigation;

interface LeftNavViewInterface
{
    /**
     * @param string $key
     * @return mixed
     */
    public function getContentOverviewUrl($key);

    /**
     * @return string
     */
    public function getContentTypeOverviewUrl();

    /**
     * @return string
     */
    public function getDashboardUrl();

    /**
     * @return string
     */
    public function getLogoutUrl();

    /**
     * @param string $module
     * @return string
     */
    public function getModuleUrl($module);

    /**
     * @return string
     */
    public function getUserOverviewUrl();

    /**
     * @return bool
     */
    public function hasContentTypes();

    /**
     * @return bool
     */
    public function isRootUser();
}