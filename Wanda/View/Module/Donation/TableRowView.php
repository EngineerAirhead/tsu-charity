<?php
namespace Airhead\Wanda\View\Module\Donation;

use Airhead\Library\Model\Donation;
use Airhead\Wanda\View\OverviewActionInterface;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class TableRowView extends View implements TableRowViewInterface, OverviewActionInterface
{
    /**
     * @var Donation
     */
    private $donation;

    /**
     * @param Donation $donation
     */
    public function __construct(Donation $donation)
    {
        parent::__construct('Wanda/Template/module/donation/table-row');

        $this->donation = $donation;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return Container::getRouter()->pathFor('module-delete', [
            'module' => 'donation',
            'id' => $this->donation->getId()
        ]);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return Container::getRouter()->pathFor('module-edit', [
            'module' => 'donation',
            'id' => $this->donation->getId()
        ]);
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->donation->getAmount();
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return date('d-m-Y H:i:s', strtotime($this->donation->getDateCreate()));
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->donation->getEmail();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->donation->getName();
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        return $this->donation->getTransactionId();
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {
        return $this->donation->getStatus() === 'confirmed';
    }
}