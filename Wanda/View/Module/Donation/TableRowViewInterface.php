<?php
namespace Airhead\Wanda\View\Module\Donation;

interface TableRowViewInterface
{
    /**
     * @return float
     */
    public function getAmount();

    /**
     * @return string
     */
    public function getDateCreated();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getTransactionId();

    /**
     * @return bool
     */
    public function isConfirmed();
}