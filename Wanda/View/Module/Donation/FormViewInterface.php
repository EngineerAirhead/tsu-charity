<?php
namespace Airhead\Wanda\View\Module\Donation;

interface FormViewInterface
{
    /**
     * @return float
     */
    public function getAmount();

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getOverviewUrl();

    /**
     * @return string
     */
    public function getTransactionId();

    /**
     * @return string
     */
    public function getValidation();

    /**
     * @return bool
     */
    public function isConfirmed();

    /**
     * @return bool
     */
    public function hasDonationData();
}