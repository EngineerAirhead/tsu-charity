<?php
namespace Airhead\Wanda\View\Module\Donation;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getAddDonationUrl();

    /**
     * @return string
     */
    public function getDonationRows();
}