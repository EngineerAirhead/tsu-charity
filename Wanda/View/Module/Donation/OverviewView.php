<?php
namespace Airhead\Wanda\View\Module\Donation;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Donation;
use Airhead\Wanda\View\DashboardContentViewInterface;

class OverviewView extends View implements DashboardContentViewInterface, OverviewViewInterface
{
    /**
     * @var Donation[]
     */
    private $donations;

    /**
     * @param Donation[] $donations
     */
    public function __construct($donations)
    {
        parent::__construct('Wanda/Template/module/donation/overview');

        $this->donations = $donations;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Piggy bank for charity';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Donations';
    }

    /**
     * @return string
     */
    public function getAddDonationUrl()
    {
        return Container::getRouter()->pathFor('module-add', ['module' => 'donation']);
    }

    /**
     * @return string
     */
    public function getDonationRows()
    {
        $return = '';
        foreach ($this->donations as $donation) {
            $return .= (new TableRowView($donation))->parse();
        }
        return $return;
    }
}