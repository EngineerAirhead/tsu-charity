<?php
namespace Airhead\Wanda\View\Module\Donation;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Donation;
use Airhead\Wanda\View\DashboardContentViewInterface;

class FormView extends View implements DashboardContentViewInterface, FormViewInterface
{
    /**
     * @var null|\string[]
     */
    private $inputData;

    /**
     * @var Donation
     */
    private $donation;

    /**
     * @var View|null
     */
    private $validationView;

    /**
     * @param Donation|null $donation
     * @param string[]|null $inputData
     * @param View|null $validationView
     */
    public function __construct(Donation $donation = null, $inputData = null, View $validationView = null)
    {
        parent::__construct('Wanda/Template/module/donation/form');

        $this->donation = $donation;
        $this->inputData = $inputData;
        $this->validationView = $validationView;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'it makes the world go round.';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        if ($this->hasDonationData() === true) {
            return 'Edit donation';
        }

        return 'Add donation';
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        if ($this->hasDonationData() === false) {
            return $this->inputData['amount'];
        }

        return $this->donation->getAmount();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        if ($this->hasDonationData() === false) {
            return $this->inputData['email'];
        }

        return $this->donation->getEmail();
    }

    /**
     * @return string
     */
    public function getName()
    {
        if ($this->hasDonationData() === false) {
            return $this->inputData['name'];
        }

        return $this->donation->getName();
    }

    /**
     * @return string
     */
    public function getOverviewUrl()
    {
        return Container::getRouter()->pathFor('module', ['module' => 'donation']);
    }

    /**
     * @return string
     */
    public function getTransactionId()
    {
        if ($this->hasDonationData() === false) {
            return 'manual__'.md5(date('Y-m-d H:i:s'));
        }

        return $this->donation->getTransactionId();
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        if ($this->validationView === null) {
            return '';
        }

        return $this->validationView->parse();
    }

    /**
     * @return bool
     */
    public function isConfirmed()
    {

        if ($this->hasDonationData() === false) {
            return false;
        }

        return $this->donation->getStatus() === 'confirmed';
    }

    /**
     * @return bool
     */
    public function hasDonationData()
    {
        return $this->donation !== null;
    }
}