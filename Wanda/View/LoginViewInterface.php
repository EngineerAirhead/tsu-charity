<?php
namespace Airhead\Wanda\View;

interface LoginViewInterface
{
    /**
     * @param string $key
     * @return string
     */
    public function getInputValue($key);

    /**
     * @return string
     */
    public function getNotificationFlashMessage();

    /**
     * @return string
     */
    public function getValidation();
}