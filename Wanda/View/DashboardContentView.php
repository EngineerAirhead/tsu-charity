<?php
namespace Airhead\Wanda\View;

use Airhead\Library\Framework\View;

class DashboardContentView extends View implements DashboardContentViewInterface, DashboardDataInterface
{
    /**
     * @var array
     */
    private $donationStatistics;

    /**
     * @param array $donationStatistics
     */
    public function __construct($donationStatistics)
    {
        parent::__construct('Wanda/Template/dashboard');

        $this->donationStatistics = $donationStatistics;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'Statistics and more';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Dashboard';
    }

    /**
     * @return string
     */
    public function getDonationStatisticsJson()
    {
        return htmlspecialchars(json_encode($this->donationStatistics), ENT_QUOTES, 'UTF-8');
    }
}