<?php
namespace Airhead\Wanda\View\Config\ContentType;

interface OverviewViewInterface
{
    /**
     * @return string
     */
    public function getAddContentTypeUrl();

    /**
     * @return string
     */
    public function getContentTypeRows();
}