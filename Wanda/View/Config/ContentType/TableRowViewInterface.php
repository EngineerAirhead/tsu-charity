<?php
namespace Airhead\Wanda\View\Config\ContentType;

interface TableRowViewInterface
{
    /**
     * @return string
     */
    public function getContentTypeCode();

    /**
     * @return int
     */
    public function getContentTypeId();

    /**
     * @return string
     */
    public function getContentTypeName();
}