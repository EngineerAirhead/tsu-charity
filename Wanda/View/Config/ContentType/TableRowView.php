<?php
namespace Airhead\Wanda\View\Config\ContentType;

use Airhead\Library\Model\ContentType;
use Airhead\Wanda\View\OverviewActionInterface;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class TableRowView extends View implements TableRowViewInterface, OverviewActionInterface
{
    /**
     * @var ContentType
     */
    private $contentType;

    /**
     * @param ContentType $contentType
     */
    public function __construct(ContentType $contentType)
    {
        parent::__construct('Wanda/Template/config/content-type/table-row');

        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getDeleteUrl() {
        return Container::getRouter()->pathFor('content-type-delete', ['id' => $this->getContentTypeId()]);
    }

    /**
     * @return string
     */
    public function getEditUrl()
    {
        return Container::getRouter()->pathFor('content-type-edit', ['id' => $this->getContentTypeId()]);
    }

    /**
     * @return string
     */
    public function getContentTypeCode()
    {
        return $this->contentType->getCode();
    }

    /**
     * @return int
     */
    public function getContentTypeId()
    {
        return $this->contentType->getId();
    }

    /**
     * @return string
     */
    public function getContentTypeName()
    {
        return $this->contentType->getName();
    }


}