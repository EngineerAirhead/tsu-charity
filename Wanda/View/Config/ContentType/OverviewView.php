<?php
namespace Airhead\Wanda\View\Config\ContentType;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\ContentType;
use Airhead\Wanda\View\DashboardContentViewInterface;

class OverviewView extends View implements OverviewViewInterface, DashboardContentViewInterface
{
    /**
     * @var ContentType[]
     */
    private $contentTypes;

    /**
     * @param ContentType[] $contentTypes
     */
    public function __construct($contentTypes)
    {
        parent::__construct('Wanda/Template/config/content-type/overview');

        $this->contentTypes = $contentTypes;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'We do need content, right?';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Content types';
    }

    /**
     * @return string
     */
    public function getAddContentTypeUrl()
    {
        return Container::getRouter()->pathFor('content-type-add');
    }

    /**
     * @return string
     */
    public function getContentTypeRows()
    {
        $return = '';
        foreach ($this->contentTypes as $contentType) {
            $return .= (new TableRowView($contentType))->parse();
        }
        return $return;
    }
}