<?php
namespace Airhead\Wanda\View\Config\ContentType;

use Countable;

interface FormViewInterface
{
    /**
     * @return string
     */
    public function getContentTypeCode();

    /**
     * @return string
     */
    public function getContentTypeName();

    /**
     * @return Countable
     */
    public function getExtraFields();

    /**
     * @return string
     */
    public function getOverviewUrl();

    /**
     * @return string
     */
    public function getValidation();

    /**
     * @param string $key
     * @return bool
     */
    public function isDefaultChecked($key);
}