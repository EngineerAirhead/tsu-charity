<?php
namespace Airhead\Wanda\View\Email;

interface EmailInterface
{
    /**
     * @return string
     */
    public function getEmailBody();

    /**
     * @return string
     */
    public function getEmailTitle();
}