<?php
namespace Airhead\Wanda\View\Email;

interface NewAccountInterface
{
    /**
     * @return string
     */
    public function getEmail();

    /**
     * @return string
     */
    public function getLoginUrl();

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @return string
     */
    public function getUsername();
}