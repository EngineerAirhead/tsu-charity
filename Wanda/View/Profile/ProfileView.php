<?php
namespace Airhead\Wanda\View\Profile;

use Airhead\Library\Framework\Gravatar;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\User;
use Airhead\Wanda\View\DashboardContentViewInterface;

class ProfileView extends View implements DashboardContentViewInterface, ProfileViewInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct('Wanda/Template/profile/form');

        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getContentSubTitle()
    {
        return 'filled with all your dirty little secrets';
    }

    /**
     * @return string
     */
    public function getContentTitle()
    {
        return 'Your profile';
    }

    /**
     * @return string
     */
    public function getFormEmailValue()
    {
        return $this->user->getEmail();
    }

    /**
     * @return string
     */
    public function getFormNameValue()
    {
        return $this->user->getName();
    }

    /**
     * @return string
     */
    public function getFormUsernameValue()
    {
        return $this->user->getUsername();
    }

    /**
     * @param int $size
     * @return string
     */
    public function getUserAvatar($size = 80)
    {
        return Gravatar::getGravatarUrl($this->user->getEmail(), $size);
    }

    /**
     * @return string
     */
    public function getValidation()
    {
        return '';
    }
}