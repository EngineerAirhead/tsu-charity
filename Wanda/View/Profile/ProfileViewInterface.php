<?php
namespace Airhead\Wanda\View\Profile;

interface ProfileViewInterface
{
    /**
     * @return string
     */
    public function getFormEmailValue();

    /**
     * @return string
     */
    public function getFormNameValue();

    /**
     * @return string
     */
    public function getFormUsernameValue();

    /**
     * @param int $size
     * @return string
     */
    public function getUserAvatar($size = 80);

    /**
     * @return string
     */
    public function getValidation();
}