<?php
namespace Airhead\Wanda\View;

interface DashboardDataInterface
{
    /**
     * @return string
     */
    public function getDonationStatisticsJson();
}