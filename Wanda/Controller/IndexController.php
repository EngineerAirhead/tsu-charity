<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Library\Repository\DonateRepository;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\DashboardContentView;
use Airhead\Wanda\View\PageView;
use Airhead\Library\Framework\Container;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class IndexController extends BaseController
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var DonateRepository
     */
    private $donateRepository;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->donateRepository = new DonateRepository(Container::getDatabase());
    }

    /**
     * @return string
     */
    protected function index()
    {
        $donationsCreated = $this->donateRepository->findSumCreatedPerHour();
        $donationsConfirmed = $this->donateRepository->findSumConfirmedPerHour();

        $createdAmount = $confirmedAmount = 0;

        $donationsCreatedArray = $donationsConfirmedArray = [];

        if (count($donationsCreated) > 0) {
            $donationsCreatedArray[] = [
                'x' => strtotime($donationsCreated[0]['date']) - 3600,
                'y' => $createdAmount
            ];
            foreach ($donationsCreated as $donationCreated) {
                $createdAmount += $donationCreated['amount'];
                $donationsCreatedArray[] = [
                    'x' => strtotime($donationCreated['date']),
                    'y' => $createdAmount
                ];
            }
        }

        if (count($donationsCreated) > 0) {
            $donationsConfirmedArray[] = [
                'x' => strtotime($donationsConfirmed[0]['date']) - 3600,
                'y' => $confirmedAmount
            ];
            foreach ($donationsConfirmed as $donationConfirmed) {
                $confirmedAmount += $donationConfirmed['amount'];
                $donationsConfirmedArray[] = [
                    'x' => strtotime($donationConfirmed['date']),
                    'y' => $confirmedAmount
                ];
            }
        }

        $firstHour = reset($donationsCreatedArray)['date'];
        $lastHour = end($donationsConfirmedArray)['date'];

        $donationStatistics = [
            'range' => ['min' => $firstHour, 'max' => $lastHour],
            'data' => [
                ['area' => true, 'key' => 'Created', 'values' => $donationsCreatedArray],
                ['key' => 'Confirmed', 'values' => $donationsConfirmedArray]
            ]
        ];

        return (new PageView($this->dashboardViewFactory->buildDashboardView(new DashboardContentView(
            $donationStatistics
        ))))->parse();
    }
}