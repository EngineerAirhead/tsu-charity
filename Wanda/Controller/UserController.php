<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Library\Framework\Random;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\User;
use Airhead\Library\Repository\RoleRepository;
use Airhead\Wanda\View\Email\Email;
use Airhead\Wanda\View\Email\NewAccount;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Airhead\Wanda\View\User\FormView;
use Airhead\Wanda\View\User\OverviewView;
use Airhead\Library\Framework\Container;
use Airhead\Library\Repository\UserRepository;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator;

class UserController extends BaseController
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->roleRepository = new RoleRepository(Container::getDatabase());
        $this->userRepository = new UserRepository(Container::getDatabase());
        $this->validation = new Validation();
    }

    /**
     * @return string
     */
    protected function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView($this->userRepository->findAll())
        )))->parse();
    }

    /**
     * @return string
     */
    protected function add()
    {
        $inputData = null;

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules([
                'username' => Validator::stringType()->notEmpty()->setName('Username'),
                'name' => Validator::stringType()->notEmpty()->setName('Full name'),
                'email' => Validator::email()->notEmpty()->setName('Email'),
                'id_user_role' => Validator::intVal(),
                'reset_password' => Validator::intVal(),
            ]);

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $duplicateStatus = $this->userRepository->findByEmail($inputData['email']);
            if ($duplicateStatus !== false) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    ['Email already exists']
                ));
            }

            // Create random password
            $password = Random::createPasswordHash(12);
            $hash = Random::createPasswordHash();

            // Add password to input data
            $inputData['password'] = sha1($hash.$password);
            $inputData['hash'] = $hash;

            $userId = $this->userRepository->create($inputData);
            if ($userId === false) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    ['Unable to create user']
                ));
            }

            // Send email with default password
            $mail = new \PHPMailer();

            $mail->setFrom('vicky@engineer-airhead.com', 'Vicky - your CMS assistant');
            $mail->addAddress($inputData['email'], $inputData['name']);
            $mail->addBCC('vicky@engineer-airhead.com', 'Vicky - your CMS assistant');

            $mail->isHTML(true);

            $mail->Subject = 'New account created in ';
            $mail->Body = (new Email(
                'Welcome to the system ' . $inputData['name'],
                new NewAccount($inputData['username'], $inputData['email'], $password))
            )->parse();

            if(!$mail->send()) {
                Container::getFlash()->addMessage(
                    'warning',
                    new NotificationView('success', ['A new user has been created, but I was unable to send an email'])
                );
            } else {
                Container::getFlash()->addMessage(
                    'notification',
                    new NotificationView('success', ['A new user has been created'])
                );
            }

            return $this->response->withRedirect(Container::getRouter()->pathFor('user-edit', ['id' => $userId]));
        }

        return $this->returnFormView(null, $inputData);
    }

    /**
     * @return string
     */
    protected function edit()
    {
        $inputData = null;

        $user = $this->userRepository->findById($this->args['id']);
        if ($user === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That user does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('index'));
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules([
                'username' => Validator::stringType()->notEmpty()->setName('Username'),
                'name' => Validator::stringType()->notEmpty()->setName('Full name'),
                'email' => Validator::email()->notEmpty()->setName('Email'),
                'id_user_role' => Validator::intVal(),
                'reset_password' => Validator::intVal(),
            ]);

            if ($inputData['email'] !== $user->getEmail()) {
                $duplicateStatus = $this->userRepository->findByEmail($inputData['email']);
                if ($duplicateStatus !== false) {
                    return $this->returnFormView($user, $inputData, new NotificationView(
                        'danger',
                        ['Email already exists']
                    ));
                }
            }

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView($user, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $status = $this->userRepository->update($user, $inputData);
            if ($status === false) {
                return $this->returnFormView($user, $inputData, new NotificationView(
                    'danger',
                    ['Unable to update user']
                ));
            }

            return $this->returnFormView($user, $inputData, new NotificationView('success', ['Updated the user']));
        }

        return $this->returnFormView($user, $inputData);
    }

    /**
     * @return mixed
     */
    protected function delete()
    {
        $user = $this->userRepository->findById($this->args['id']);
        if ($user === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That user does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('user'));
        }

        $status = $this->userRepository->delete($user);
        if ($status === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Could not remove the user.'])
            );
        } else {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['The user has been removed.'])
            );
        }

        return $this->response->withRedirect(Container::getRouter()->pathFor('user'));
    }

    /**
     * @param User|null $user
     * @param string[]|null $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function returnFormView(User $user = null, $inputData = null, View $notificationView = null)
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new FormView($this->roleRepository->findAll(), $user, $inputData, $notificationView)
        )))->parse();
    }
}