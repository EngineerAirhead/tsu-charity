<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Framework\Random;
use Airhead\Wanda\View\LoginResetView;
use Airhead\Wanda\View\LoginView;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Framework\View;
use Airhead\Library\Repository\UserRepository;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Respect\Validation\Validator;

class LoginController extends BaseController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->userRepository = new UserRepository(Container::getDatabase());
        $this->validation = new Validation();
    }

    /**
     * @return string
     */
    protected function index()
    {
        $inputData = null;

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules([
                'email' => Validator::email()->notEmpty()->setName('Email'),
                'password' => Validator::stringType()->notEmpty()->setName('Password')
            ]);

            if ($this->validation->isValid($inputData) !== true) {
                return $this->renderOutput($inputData, new NotificationView('danger', $this->validation->getErrors()));
            }

            $user = $this->userRepository->findByEmail($inputData['email']);
            if ($user === false) {
                return $this->renderOutput($inputData, new NotificationView('danger', ['User does not exist']));
            }

            if (sha1($user->getHash().$inputData['password']) !== $user->getPassword()) {
                return $this->renderOutput($inputData, new NotificationView('danger', ['Password is incorrect']));
            }

            Container::getSession()->set('admin', $user);
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['You are now logged in'])
            );

            if ($user->shouldResetPassword() === true) {
                Container::getSession()->set(
                    'redirectUrl',
                    Container::getConfig()->get('baseUri').Container::getRouter()->pathFor('login-reset')
                );
            }

            $redirectUrl = Container::getRouter()->pathFor('index');
            if (Container::getSession()->get('redirectUrl') !== false) {
                $redirectUrl = Container::getSession()->get('redirectUrl');
                Container::getSession()->remove('redirectUrl');
            }

            return $this->response->withRedirect($redirectUrl);
        }

        return $this->renderOutput($inputData);
    }

    /**
     * @return string
     */
    protected function reset()
    {
        $inputData = null;

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();

            $this->validation->setRules([
                'password_old' => Validator::stringType()->notEmpty()->setName('Old password'),
                'password_new' => Validator::stringType()->notEmpty()->setName('New password'),
                'password_new_2' => Validator::stringType()->notEmpty()->setName('Confirm new password')
            ]);

            if ($this->validation->isValid($inputData) !== true) {
                return $this->renderResetOutput($inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            if ($inputData['password_new'] !== $inputData['password_new_2']) {
                return $this->renderResetOutput($inputData, new NotificationView(
                    'danger',
                    ['New password does not match confirmation']
                ));
            }

            $password = sha1($this->adminLoggedIn->getHash().$inputData['password_old']);
            if ($password !== $this->adminLoggedIn->getPassword()) {
                return $this->renderResetOutput($inputData, new NotificationView('danger', ['Password is incorrect']));
            }

            $newHash = Random::createPasswordHash();

            $newUserData = $this->userRepository->updatePassword($this->adminLoggedIn, [
                'password' => sha1($newHash.$inputData['password_new']),
                'hash' => $newHash,
                'reset_password' => null
            ]);
            if ($newUserData === false) {
                return $this->renderResetOutput($inputData, new NotificationView(
                    'danger',
                    ['Unable to update password']
                ));
            }

            Container::getSession()->set('admin', $newUserData);
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['Your password has been updated'])
            );

            $redirectUrl = Container::getRouter()->pathFor('index');
            if (Container::getSession()->get('redirectUrl') !== false) {
                $redirectUrl = Container::getSession()->get('redirectUrl');
                Container::getSession()->remove('redirectUrl');
            }

            return $this->response->withRedirect($redirectUrl);
        }

        return $this->renderResetOutput($inputData);
    }

    /**
     * @return Response
     */
    protected function logout()
    {
        Container::getSession()->remove('admin');
        Container::getFlash()->addMessage(
            'notification',
            new NotificationView('success', ['You are now logged out'])
        );
        return $this->response->withRedirect(Container::getRouter()->pathFor('login'));
    }

    /**
     * @param string[] $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function renderOutput($inputData, View $notificationView = null)
    {
        return (new PageView(
            new LoginView($inputData, $notificationView, Container::getFlash()->getMessage('notification'))
        ))->parse();
    }

    /**
     * @param string[] $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function renderResetOutput($inputData, View $notificationView = null)
    {
        return (new PageView(
            new LoginResetView($inputData, $notificationView, Container::getFlash()->getMessage('notification'))
        ))->parse();
    }
}