<?php
namespace Airhead\Wanda\Controller\Module;

interface ModuleControllerInterface
{
    /**
     * @return string
     */
    public function index();

    /**
     * @return string
     */
    public function add();

    /**
     * @return string
     */
    public function edit();

    /**
     * @return string
     */
    public function delete();
}