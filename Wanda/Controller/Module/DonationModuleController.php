<?php
namespace Airhead\Wanda\Controller\Module;

use Airhead\Library\Factory\EmailFactory;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Push;
use Airhead\Library\Framework\Validation;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Donation;
use Airhead\Library\Repository\ContentRepository;
use Airhead\Library\Repository\ContentTypeRepository;
use Airhead\Library\Repository\DonateRepository;
use Airhead\Wanda\Controller\BaseController;
use Airhead\Wanda\Factory\DashboardViewFactory;
use Airhead\Wanda\View\Module\Donation\FormView;
use Airhead\Wanda\View\Module\Donation\OverviewView;
use Airhead\Wanda\View\NotificationView;
use Airhead\Wanda\View\PageView;
use Respect\Validation\Validator;
use Slim\Http\Request;
use Slim\Http\Response;

class DonationModuleController extends BaseController implements ModuleControllerInterface
{
    /**
     * @var DashboardViewFactory
     */
    private $dashboardViewFactory;

    /**
     * @var DonateRepository
     */
    private $donateRepository;

    /**
     * @var EmailFactory
     */
    private $emailFactory;

    /**
     * @var Push
     */
    private $pushHandler;

    /**
     * @var Validation
     */
    private $validation;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->dashboardViewFactory = new DashboardViewFactory(new ContentTypeRepository(Container::getDatabase()));
        $this->donateRepository = new DonateRepository(Container::getDatabase());
        $this->emailFactory = new EmailFactory(
            new ContentRepository(Container::getDatabase()),
            Container::getConfig()->get('projectRoot')
        );
        $this->validation = new Validation();
        $this->pushHandler = new Push(Container::getConfig()->get('pushBullet')['token']);
    }

    public function index()
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new OverviewView($this->donateRepository->findAll())
        )))->parse();
    }

    /**
     * @return string
     */
    public function add()
    {
        $inputData = null;

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();
            if ($inputData['amount'] < 1) {
                $inputData['amount'] = 1;
            }

            $this->validation->setRules([
                'name' => Validator::stringType()->notEmpty()->setName('Name'),
                'email' => Validator::email()->notEmpty()->setName('Email'),
                'amount' => Validator::intVal()->notEmpty()->setName('Amount'),
                'transaction_id' => Validator::stringType()->notEmpty()->setName('Transaction ID'),
            ]);

            if ($this->validation->isValid($inputData) !== true) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    $this->validation->getErrors()
                ));
            }

            $inputData['date_create'] = $inputData['date_update'] = date('Y-m-d H:i:s');
            $inputData['status'] = 'confirmed';

            $donation = $this->donateRepository->create($inputData);
            if ($donation === false) {
                return $this->returnFormView(null, $inputData, new NotificationView(
                    'danger',
                    ['Unable to create donation']
                ));
            }

            if ($this->emailFactory->sendDonationConfirmedEmail($donation) === false) {
                Container::getFlash()->addMessage(
                    'notification',
                    new NotificationView('info', ['We could not send the confirmation email.'])
                );
            }

            $this->pushHandler->pushData($donation->asArray());

            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['A new donation has been created.'])
            );

            return $this->response->withRedirect(
                Container::getRouter()->pathFor('module-edit', ['id' => $donation->getId(), 'module' => 'donation'])
            );
        }

        return $this->returnFormView(null, $inputData);
    }

    /**
     * @return string
     */
    public function edit()
    {
        $inputData = null;

        $donation = $this->donateRepository->findById($this->args['id']);
        if ($donation === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That donation does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('index'));
        }

        if ($this->request->getMethod() === 'POST') {
            $inputData = $this->request->getParsedBody();
            if (isset($inputData['confirmed']) === false) {
                return $this->returnFormView($donation, $inputData);
            }

            $status = $this->donateRepository->confirmTransaction($donation->getTransactionId());
            if ($status === false) {
                return $this->returnFormView($donation, $inputData, new NotificationView(
                    'danger',
                    ['Unable to confirm the donation']
                ));
            }

            $confirmMessage = 'Confirmed the donation';
            if ($this->emailFactory->sendDonationConfirmedEmail($donation) === false) {
                $confirmMessage .= ', but we were unable to send the confirmation email';
            }

            $donation->setStatus('confirmed');

            $this->pushHandler->pushData($donation->asArray());

            return $this->returnFormView($donation, $inputData, new NotificationView('success', [$confirmMessage]));
        }

        return $this->returnFormView($donation, $inputData);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete()
    {
        $donation = $this->donateRepository->findById($this->args['id']);
        if ($donation === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['That donation does not even exist yet!'])
            );
            return $this->response->withRedirect(Container::getRouter()->pathFor('user'));
        }

        $status = $this->donateRepository->delete($donation);
        if ($status === false) {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('danger', ['Could not remove the donation.'])
            );
        } else {
            Container::getFlash()->addMessage(
                'notification',
                new NotificationView('success', ['The donation has been removed.'])
            );
        }

        return $this->response->withRedirect(Container::getRouter()->pathFor('module', ['module' => 'donation']));
    }

    /**
     * @param Donation|null $donation
     * @param string[]|null $inputData
     * @param View|null $notificationView
     * @return string
     */
    private function returnFormView(Donation $donation = null, $inputData = null, View $notificationView = null)
    {
        return (new PageView($this->dashboardViewFactory->buildDashboardView(
            new FormView($donation, $inputData, $notificationView)
        )))->parse();
    }
}