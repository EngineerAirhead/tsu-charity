<?php
namespace Airhead\Wanda\Controller;

use Airhead\Wanda\Controller\Module\ModuleControllerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

class ModuleController extends BaseController implements ModuleControllerInterface
{
    /**
     * @var ModuleControllerInterface
     */
    private $module;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $moduleController = 'Airhead\\Wanda\\Controller\\Module\\'.ucfirst($args['module']).'ModuleController';
        $this->module = new $moduleController($request, $response, $args);
    }

    /**
     * @return string
     */
    public function index()
    {
        return $this->module->index();
    }

    /**
     * @return string
     */
    public function add()
    {
        return $this->module->add();
    }

    /**
     * @return string
     */
    public function ajax()
    {
        $call = 'ajax' . ucfirst($this->args['call']);

        if (method_exists($this->module, $call) === true) {
            return call_user_func_array([$this->module, $call], $this->args);
        }

        return $this->renderError('Ajax call [' . $call . '] not supported in [' . get_class($this->module) . ']');
    }

    /**
     * @return string
     */
    public function edit()
    {
        return $this->module->edit();
    }

    /**
     * @return string
     */
    public function delete()
    {
        return $this->module->delete();
    }
}