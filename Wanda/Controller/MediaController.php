<?php
namespace Airhead\Wanda\Controller;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\FileHelper;
use Airhead\Library\Framework\Sanitize;
use Slim\Http\UploadedFile;

class MediaController extends BaseController
{
    protected function upload()
    {
        $files = $this->request->getUploadedFiles();

        // Check if we have files to process
        if (count($files) < 1 || isset($files['file']) === false) {
            return $this->renderError('File missing');
        }

        /** @var UploadedFile $fileObject */
        $fileObject = $files['file'];

        // Check for errors
        if ($fileObject->getError() > 0 && $fileObject->getError() !== 4 &&
            $fileObject->getClientFilename() !== ''
        ) {
            return $this->renderError(FileHelper::getFileUploadErrorMsg($fileObject->getError()));
        }

        // Check for empty file
        if ($fileObject->getError() === 4 || $fileObject->getClientFilename() === '') {
            return $this->renderError('File missing');
        }

        // Check for valid file type
        if (FileHelper::isValidImageFile($fileObject->file) !== true) {
            return $this->renderError($fileObject->getClientFilename().' needs to be a valid image file');
        }

        // Prepare file variables
        $directory = 'public'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR;
        $fileName = Sanitize::sanitizeFileNameString($fileObject->getClientFilename());

        FileHelper::uploadFile(
            $fileObject,
            Container::getConfig()->get('baseDir').DIRECTORY_SEPARATOR.$directory,
            $fileName
        );

        $url = str_replace('vicky/', '', Container::getConfig()->get('basePath'));

        return $this->renderSuccess([
            'filename' => $fileName,
            'url' => $url.str_replace(DIRECTORY_SEPARATOR, '/', $directory).$fileName
        ]);
    }
}