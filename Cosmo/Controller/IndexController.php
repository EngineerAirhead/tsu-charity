<?php
namespace Airhead\Cosmo\Controller;

use Airhead\Cosmo\View\SponsorBlockView;
use Airhead\Cosmo\View\SponsorsView;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\ViewCollection;
use Airhead\Library\Model\Content;
use Airhead\Library\Model\Streamer;
use Airhead\Library\Repository\ContentRepository;
use Airhead\Cosmo\View\PageView;
use Airhead\Library\Repository\DonateRepository;
use CurlHelper;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class IndexController extends BaseController
{
    /**
     * @var ContentRepository
     */
    private $contentRepository;

    /**
     * @var DonateRepository
     */
    private $donationRepository;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->contentRepository = new ContentRepository(Container::getDatabase());
        $this->donationRepository = new DonateRepository(Container::getDatabase());
    }

    /**
     * @throws \Exception
     */
    public function index()
    {
        echo (new PageView(
            $this->getStreamers(),
            $this->donationRepository->getTotalDonationAmount(),
            $this->donationRepository->getTopDonators(10),
            $this->getSponsorView($this->contentRepository->findContentByType('sponsor')),
            $this->contentRepository->findContentByType('news', true, 5, 'content.date_create|DESC'),
            Container::getFlash()->getMessage('notification')
        ))->parse();
    }

    /**
     * @param Content[] $sponsors
     * @return SponsorsView
     */
    private function getSponsorView($sponsors)
    {
        $sponsorViewCollection = new ViewCollection();

        if (count($sponsors) > 0) {
            foreach ($sponsors as $sponsor) {
                $sponsorViewCollection->addSubView(new SponsorBlockView($sponsor));
            }
        }

        return new SponsorsView($sponsorViewCollection);
    }

    /**
     * @return array
     */
    private function getStreamers()
    {
        $return = [];
        $streamers = $this->contentRepository->findContentByType('streamer');
        if (count($streamers) > 0) {
            $links = array_map(function ($data) {
                /** @var Content $data */
                return $data->getContentData()['link']->getData();
            }, $streamers);

            $onlineStatus = $this->getOnlineStatus($links);

            foreach ($streamers as $streamer) {
                $link = $streamer->getContentData()['link']->getData();

                // Get avatar url
                $rawData = CurlHelper::factory('https://api.twitch.tv/kraken/users/' . $link)
                    ->setGetParams([
                        'client_id' => '2bvk42tz2abunb9p6sxvwjppr4znc03'
                    ])
                    ->exec();

                $result = json_decode($rawData['content']);

                $return[] = new Streamer(
                    $streamer->getId(),
                    $streamer->getContentInformation()->getTitle(),
                    $link,
                    $onlineStatus[$link]['game'],
                    $result->logo,
                    $onlineStatus[$link]['status']
                );
            }
        }

        return $return;
    }

    /**
     * @param array $links
     * @return array
     */
    private function getOnlineStatus($links)
    {
        $returnData = array_combine($links, array_map(function () {
            return ['status' => 'offline', 'game' => ''];
        }, $links));

        $rawData = CurlHelper::factory('https://api.twitch.tv/kraken/streams')
            ->setGetParams([
                'client_id' => '2bvk42tz2abunb9p6sxvwjppr4znc03',
                'channel' => implode(',', $links)
            ])
            ->exec();

        $result = json_decode($rawData['content']);
        if ($result->_total > 0) {
            foreach ($result->streams as $stream) {
                $returnData[$stream->channel->name] = [
                    'status' => 'online',
                    'game' => $stream->game
                ];
            }
        }

        return $returnData;
    }
}