<?php
namespace Airhead\Cosmo\Controller;

use Airhead\Library\Framework\Container;
use Airhead\Library\Repository\NewsletterRepository;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class NewsletterController extends BaseController
{
    /**
     * @var NewsletterRepository
     */
    private $newsletterRepository;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->newsletterRepository = new NewsletterRepository(Container::getDatabase());
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $email = $this->request->getParsedBody()['email'];
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return $this->renderError('Email is invalid');
        }

        if ($this->newsletterRepository->isDuplicateEmail($email) === true) {
            return $this->renderError('We already have you on our list');
        }

        if ($this->newsletterRepository->insertEmail($email) !== false) {
            return $this->renderSuccess('We will keep you informed!');
        }

        return $this->renderError('Something went wrong, and we don\'t know what');
    }
}