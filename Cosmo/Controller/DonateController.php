<?php
namespace Airhead\Cosmo\Controller;

use Airhead\Cosmo\View\DonateSummaryView;
use Airhead\Cosmo\View\NotificationView;
use Airhead\Library\Factory\EmailFactory;
use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\Push;
use Airhead\Library\Repository\ContentRepository;
use Airhead\Library\Repository\DonateRepository;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class DonateController extends BaseController
{
    /**
     * @var ContentRepository
     */
    private $contentRepository;

    /**
     * @var DonateRepository
     */
    private $donateRepository;

    /**
     * @var EmailFactory
     */
    private $emailFactory;

    /**
     * @var Push
     */
    private $pushHandler;

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);

        $this->donateRepository = new DonateRepository(Container::getDatabase());
        $this->contentRepository = new ContentRepository(Container::getDatabase());
        $this->emailFactory = new EmailFactory($this->contentRepository, Container::getConfig()->get('projectRoot'));
        $this->pushHandler = new Push(Container::getConfig()->get('pushBullet')['token']);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $inputData = $this->request->getParsedBody();
        array_walk($inputData, 'trim');

        if ($inputData != array_filter($inputData)) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> Required fields are empty</div>');
        }

        if (filter_var($inputData['email'], FILTER_VALIDATE_EMAIL) === false) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> Email is invalid</div>');
        }

        if ($inputData['amount'] < 1) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> Donation amount needs to be at least 1 dollar</div>');
        }

        $createDate = date('Y-m-d H:i:s');

        // Create database object
        $donationObject = $this->donateRepository->create([
            'transaction_id' => sha1($inputData['email'].$createDate),
            'name' => $inputData['name'],
            'email' => $inputData['email'],
            'amount' => $inputData['amount'],
            'status' => 'created',
            'date_create' => $createDate
        ]);

        if ($donationObject === false) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> We could not create the donation at this time</div>');
        }

        // Create new payer object
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $donation = new Item();
        $donation->setName('The Gaming Mammoth - donation')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setPrice($donationObject->getAmount());

        $itemList = new ItemList();
        $itemList->setItems([$donation]);

        $amount = new Amount();
        $amount->setCurrency('USD')
            ->setTotal($donationObject->getAmount());

        $router = Container::getRouter();
        $baseUri = Container::getConfig()->get('baseUri');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($baseUri.$router->pathFor('donate-success'))
            ->setCancelUrl($baseUri.$router->pathFor('donate-error'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('The Gaming Mammoth - donation')
            ->setInvoiceNumber($donationObject->getTransactionId());

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction]);

        try {
            $payment->create($this->getPayPalApiContext());
        } catch (\Exception $e) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>BIG Oops!</strong> '.$e->getMessage().'</div>');
        }

        $redirectUrl = $payment->getApprovalLink();

        $successMessage = '
            <div class="alert alert-success" role="alert">
                <strong>WHOOOOP!!!</strong>
                We will open a new window to handle your donation.
                If nothing happens, please click <a href="'.$redirectUrl.'" target="_blank">here</a>!
            </div>
        ';

        return $this->renderSuccess(['redirect_url' => $redirectUrl, 'message' => $successMessage]);
    }

    /**
     * @return mixed
     */
    public function success()
    {
        $inputData = $this->request->getQueryParams();

        $payment = Payment::get($inputData['paymentId'], $this->getPayPalApiContext());

        $execution = new PaymentExecution();
        $execution->setPayerId($inputData['PayerID']);

        try {
            $payment->execute($execution, $this->getPayPalApiContext());

            try {
                $payment = Payment::get($inputData['paymentId'], $this->getPayPalApiContext());

                if ($payment->getState() === 'approved') {
                    // Loop through transactions
                    foreach ($payment->getTransactions() as $transaction) {
                        // Get the donation
                        $donation = $this->donateRepository->getDonationByTransactionId($transaction->getInvoiceNumber());
                        if ($donation === false) {
                            return $this->redirectWithMessage('danger', ['We are unable to find this donation.'], 'index');
                        }

                        // Update database record
                        $confirmation = $this->donateRepository->confirmTransaction($donation->getTransactionId());
                        if ($confirmation === false) {
                            return $this->redirectWithMessage('danger', ['We could not update the donation at this time.'], 'index');
                        }

                        if ($this->emailFactory->sendDonationConfirmedEmail($donation) === false) {
                            Container::getFlash()->addMessage(
                                'notification',
                                new NotificationView('info', ['We could not send the confirmation email.'])
                            );
                        }

                        $this->pushHandler->pushData($donation->asArray());
                    }
                }
            } catch (\Exception $e) {
                return $this->redirectWithMessage('danger', [$e->getMessage()], 'index');
            }

        } catch (\Exception $e) {
            return $this->redirectWithMessage('danger', [$e->getMessage()], 'index');
        }

        // Redirect to homepage with flash message
        return $this->redirectWithMessage('success', ['Your donation has been completed! Thank you very much!!!'], 'index');
    }

    /**
     * @return mixed
     */
    public function error()
    {
        return $this->redirectWithMessage('danger', ['Oh... Okay... Guess not :('], 'index');
    }

    /**
     * @return string
     */
    public function notify()
    {
        return (new DonateSummaryView(
            $this->donateRepository->getTotalDonationAmount(),
            count($this->donateRepository->findAllConfirmed())
        ))->parse();
    }

    /**
     * @param string $type
     * @param string[] $message
     * @param string $path
     * @return mixed
     */
    private function redirectWithMessage($type, array $message, $path)
    {
        // Redirect to homepage with flash message
        Container::getFlash()->addMessage('notification', new NotificationView($type, $message));
        return $this->response->withRedirect(Container::getRouter()->pathFor($path));
    }

    /**
     * @return ApiContext
     */
    private function getPayPalApiContext()
    {
        $payPalConfig = Container::getConfig()->get('paypal');

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $payPalConfig['clientId'],
                $payPalConfig['clientSecret']
            )
        );

        $apiContext->setConfig([
            'mode' => (Container::getConfig()->has('debug') === true) ? 'sandbox' : 'live',
            'log.LogEnabled' => true,
            'log.FileName' => '../PayPal.log',
            'log.LogLevel' => (Container::getConfig()->has('debug') === true) ? 'DEBUG' : 'FINE',
            'cache.enabled' => false,
            'http.CURLOPT_CONNECTTIMEOUT' => 30
        ]);

        return $apiContext;
    }
}