<?php
namespace Airhead\Cosmo\Controller;

use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ContactController extends BaseController
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        parent::__construct($request, $response, $args);
    }

    public function index()
    {
        $inputData = $this->request->getParsedBody();
        array_walk($inputData, 'trim');

        if ($inputData != array_filter($inputData)) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> Required fields are empty</div>');
        }

        $mail = new \PHPMailer();

        $mail->setFrom('gamingmarathon@toysoldiersunite.com', 'TSU Gaming Marathon');
        $mail->addAddress($inputData['email'], $inputData['name']);
        $mail->addBCC('gamingmarathon@toysoldiersunite.com');

        // Embed main image
        $mail->addEmbeddedImage('public/img/logo_email.png', 'GM-logo', 'logo_email.png');

        $mail->isHTML(true);

        // Get email template
        $template = file_get_contents('Cosmo/Template/email.phtml');

        $title = 'Thank you for your time';
        $body = '
            <p>We will email you back as soon as possible regarding the information below.</p>
            <hr />
            <p><strong>Subject:</strong><br />'.$inputData['subject'].'</p>
            <p><strong>Message:</strong><br />'.$inputData['comments'].'</p>
        ';

        $mail->Subject = 'Contact form - The Gaming Mammoth';
        $mail->Body    = str_replace(array('[title]', '[body]'), array($title, $body), $template);

        if(!$mail->send()) {
            return $this->renderError('<div class="alert alert-danger" role="alert"><strong>Oops!</strong> We were unable to send the confirmation ('.$mail->ErrorInfo.')</div>');
        }

        return $this->renderSuccess('<div class="alert alert-success" role="alert"><strong>Yes!</strong> We have gotten your email and will reply as soon as possible</div>');
    }
}