<?php
namespace Airhead\Cosmo\Controller;

use Airhead\Library\Framework\Container;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class BaseController
{
    /**
     * @var bool
     */
    protected $adminLoggedIn;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     */
    public function __construct(Request $request, Response $response, $args)
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        $this->adminLoggedIn = Container::getSession()->get('admin');
        $this->baseUrl = (string)$this->request->getUri()->getBasePath();
    }

    /**
     * @param mixed $feedback
     * @return mixed
     */
    public function renderSuccess($feedback)
    {
        return $this->response->withJson(
            ['status' => 'success', 'feedback' => $feedback],
            null,
            JSON_HEX_QUOT | JSON_HEX_TAG
        );
    }

    /**
     * @param string $feedback
     * @return mixed
     */
    public function renderError($feedback)
    {
        return $this->response->withJson(['status' => 'error', 'feedback' => $feedback]);
    }
}