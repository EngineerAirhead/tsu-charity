<?php
namespace Airhead\Cosmo\View;

interface DonateSummaryViewInterface
{
    /**
     * @return string
     */
    public function getBaseUrl();

    /**
     * @return float
     */
    public function getDonateProgress();

    /**
     * @return int
     */
    public function getDonateTarget();

    /**
     * @return int
     */
    public function getTotalDonateAmount();

    /**
     * @return int
     */
    public function getTotalDonations();

    /**
     * @return string
     */
    public function getWebsSocketUrl();
}