<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\FileHelper;
use Airhead\Library\Framework\Image;
use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;
use Airhead\Library\Model\Donation;
use Airhead\Library\Model\Streamer;
use Airhead\Cosmo\View\News\BodyNewsView;
use Airhead\Cosmo\View\News\FooterNewsView;

class PageView extends View implements PageViewInterface
{

    /**
     * @var Content[]
     */
    private $news;

    /**
     * @var View
     */
    private $sponsorView;

    /**
     * @var Streamer[]
     */
    private $streamers;

    /**
     * @var array
     */
    private $flashMessages;

    /**
     * @var int
     */
    private $totalDonations;

    /**
     * @var Donation[]|array
     */
    private $topDonators;

    /**
     * @param string $streamers
     * @param int $totalDonations
     * @param Donation[] $topDonators
     * @param View $sponsorView
     * @param Content[] $news
     * @param array|null $flashMessages
     */
    public function __construct($streamers, $totalDonations, array $topDonators, View $sponsorView, $news, array $flashMessages = null)
    {
        parent::__construct('Cosmo/Template/page');

        $this->streamers = $streamers;
        $this->totalDonations = (int)$totalDonations;
        $this->topDonators = $topDonators;
        $this->sponsorView = $sponsorView;
        $this->news = $news;
        $this->flashMessages = $flashMessages;
    }

    /**
     * @return string
     */
    public function getDonationForm()
    {
        //return (new DonationFormView(Container::getRouter()->pathFor('donate')))->parse();
        return '';
    }

    /**
     * @return string
     */
    public function getFlashMessages()
    {
        if ($this->flashMessages === null) {
            return '';
        }

        return implode('', $this->flashMessages);
    }

    /**
     * @return bool
     */
    public function hasFlashMessages()
    {
        return count($this->flashMessages) > 0;
    }

    /**
     * @return string
     */
    public function getFooterNews()
    {
        $return = '';
        foreach ($this->news as $news) {
            $return .= (new FooterNewsView($news))->parse();
        }
        return $return;
    }

    /**
     * @return string[]
     */
    public function getGalleryImages()
    {
        $images = FileHelper::randomFiles('public/img/slider-header', 12);
        $return = [];
        if (count($images) > 0) {
            foreach ($images as $imagePath) {
                $image = new Image($imagePath);

                $return[] = [
                    'origin' => $imagePath,
                    'thumb' => $image->getImagePath(75, 75),
                    'dim' => implode('x', $image->getDimensions())
                ];
            }
        }

        return $return;
    }

    /**
     * @return string
     */
    public function getListNews()
    {
        $return = '';
        foreach ($this->news as $news) {
            $return .= (new BodyNewsView($news))->parse();
        }
        return $return;
    }

    /**
     * @return string
     */
    public function getSponsors()
    {
        return $this->sponsorView->parse();
    }

    /**
     * @return Streamer[]
     */
    public function getStreamers()
    {
        return $this->streamers;
    }

    /**
     * @return int
     */
    public function getTargetDonationAmount()
    {
        return (int)Container::getConfig()->get('donationTarget');
    }

    /**
     * @return Donation[]
     */
    public function getTopDonators()
    {
        return $this->topDonators;
    }

    /**
     * @return int
     */
    public function getTotalDonationAmount()
    {
        return $this->totalDonations;
    }

    /**
     * @return float
     */
    public function getTotalDonationPercentage()
    {
        return round(($this->getTotalDonationAmount()/$this->getTargetDonationAmount())*100, 2);
    }

    /**
     * @return bool
     */
    public function shouldDisplayDonateButton()
    {
        return false;
    }
}