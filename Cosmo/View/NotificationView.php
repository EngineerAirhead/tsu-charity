<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;

class NotificationView extends View implements NotificationViewInterface
{
    /**
     * @var string[]
     */
    private $messages;

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $type
     * @param $messages
     */
    public function __construct($type, $messages)
    {
        parent::__construct('Cosmo/Template/notification');

        $this->type = (string)$type;
        $this->messages = $messages;
    }

    /**
     * @return \string[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

}