<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;
use Airhead\Library\Framework\ViewCollection;

class SponsorsView extends View implements SponsorsViewInterface
{
    /**
     * @var ViewCollection
     */
    private $sponsorViewCollection;

    /**
     * SponsorsView constructor.
     * @param ViewCollection $sponsorViewCollection
     */
    public function __construct(ViewCollection $sponsorViewCollection)
    {
        parent::__construct('Cosmo/Template/sponsors');

        $this->sponsorViewCollection = $sponsorViewCollection;
    }

    /**
     * @return View[]
     */
    public function getSponsorBlocks()
    {
        return $this->sponsorViewCollection->getCollection();
    }
}