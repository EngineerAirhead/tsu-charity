<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;

class DonationEmailView extends View implements DonationEmailViewInterface
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $sponsors;

    /**
     * @param string $name
     * @param string $sponsors
     * @param string $baseUrl
     */
    public function __construct($name, $sponsors, $baseUrl)
    {
        parent::__construct('Cosmo/Template/donation-email');

        $this->name = (string)$name;
        $this->sponsors = (string)$sponsors;
        $this->baseUrl = (string)$baseUrl;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSponsors()
    {
        return $this->sponsors;
    }
}