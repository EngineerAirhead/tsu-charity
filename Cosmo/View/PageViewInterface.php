<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Model\Donation;
use Airhead\Library\Model\Streamer;

interface PageViewInterface
{
    /**
     * @return string
     */
    public function getDonationForm();

    /**
     * @return string
     */
    public function getFlashMessages();

    /**
     * @return bool
     */
    public function hasFlashMessages();

    /**
     * @return string
     */
    public function getFooterNews();

    /**
     * @return string[]
     */
    public function getGalleryImages();

    /**
     * @return string
     */
    public function getListNews();

    /**
     * @return string
     */
    public function getSponsors();

    /**
     * @return Streamer[]
     */
    public function getStreamers();

    /**
     * @return int
     */
    public function getTargetDonationAmount();

    /**
     * @return Donation[]
     */
    public function getTopDonators();

    /**
     * @return int
     */
    public function getTotalDonationAmount();

    /**
     * @return float
     */
    public function getTotalDonationPercentage();

    /**
     * @return bool
     */
    public function shouldDisplayDonateButton();
}