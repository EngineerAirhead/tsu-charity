<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;

interface SponsorsViewInterface
{
    /**
     * @return View[]
     */
    public function getSponsorBlocks();
}