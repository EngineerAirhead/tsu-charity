<?php
namespace Airhead\Cosmo\View;

interface SponsorBlockViewInterface
{
    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return string
     */
    public function getImageUrl();

    /**
     * @return string
     */
    public function getLink();

    /**
     * @return string
     */
    public function getTitle();
}