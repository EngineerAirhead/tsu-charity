<?php
namespace Airhead\Cosmo\View\News;

interface FooterNewsViewInterface
{
    /**
     * @return string
     */
    public function getAuthor();

    /**
     * @return string
     */
    public function getBody();

    /**
     * @param string $format
     * @return string
     */
    public function getDate($format);

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();
}