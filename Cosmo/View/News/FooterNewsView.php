<?php
namespace Airhead\Cosmo\View\News;

use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;

class FooterNewsView extends View implements FooterNewsViewInterface
{
    /**
     * @var Content
     */
    private $content;

    /**
     * @param Content $content
     */
    public function __construct(Content $content)
    {
        parent::__construct('Cosmo/Template/news/footer');

        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->content->getUser()->getName();
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->content->getContentInformation()->getBody();
    }

    /**
     * @param string $format
     * @return string
     */
    public function getDate($format)
    {
        return date($format, strtotime($this->content->getDateCreate()));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->content->getId();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->content->getContentInformation()->getTitle();
    }
}