<?php
namespace Airhead\Cosmo\View\News;

interface BodyNewsViewInterface
{
    /**
     * @return string
     */
    public function getAuthor();

    /**
     * @param string $format
     * @return string
     */
    public function getDate($format);

    /**
     * @return int
     */
    public function getId();

    /**
     * @return string
     */
    public function getTitle();
}