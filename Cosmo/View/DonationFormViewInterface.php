<?php
namespace Airhead\Cosmo\View;

interface DonationFormViewInterface
{
    /**
     * @return string
     */
    public function getPayPalCallUrl();
}