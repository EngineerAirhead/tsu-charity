<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;

class DonationFormView extends View implements DonationFormViewInterface
{
    /**
     * @var string
     */
    private $payPalCallUrl;

    /**
     * @param string $payPalCallUrl
     */
    public function __construct($payPalCallUrl)
    {
        parent::__construct('Cosmo/Template/donate-form');

        $this->payPalCallUrl = (string)$payPalCallUrl;
    }

    /**
     * @return string
     */
    public function getPayPalCallUrl()
    {
        return $this->payPalCallUrl;
    }
}