<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\Container;
use Airhead\Library\Framework\View;

class DonateSummaryView extends View implements DonateSummaryViewInterface
{
    /**
     * @var int
     */
    private $totalDonationAmount;
    /**
     * @var int
     */
    private $totalDonations;

    /**
     * @param int $totalDonationAmount
     * @param int $totalDonations
     */
    public function __construct($totalDonationAmount, $totalDonations)
    {
        parent::__construct('Cosmo/Template/donate-summary');

        $this->totalDonationAmount = (int)$totalDonationAmount;
        $this->totalDonations = (int)$totalDonations;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return (string)Container::getConfig()->get('projectRoot');
    }

    /**
     * @return float
     */
    public function getDonateProgress()
    {
        return round(($this->getTotalDonateAmount()/$this->getDonateTarget())*100, 2);
    }

    /**
     * @return int
     */
    public function getDonateTarget()
    {
        return (int)Container::getConfig()->get('donationTarget');
    }

    /**
     * @return int
     */
    public function getTotalDonateAmount()
    {
        return $this->totalDonationAmount;
    }

    /**
     * @return int
     */
    public function getTotalDonations()
    {
        return $this->totalDonations;
    }

    /**
     * @return string
     */
    public function getWebsSocketUrl()
    {
        return (string)Container::getConfig()->get('pushBullet')['websocket'];
    }
}