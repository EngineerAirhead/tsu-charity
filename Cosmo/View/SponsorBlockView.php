<?php
namespace Airhead\Cosmo\View;

use Airhead\Library\Framework\View;
use Airhead\Library\Model\Content;
use Airhead\Library\Model\ContentData;
use Airhead\Library\Model\File;

class SponsorBlockView extends View implements SponsorBlockViewInterface
{
    /**
     * @var Content
     */
    private $sponsor;

    /**
     * SponsorBlockView constructor.
     * @param Content $sponsor
     */
    public function __construct(Content $sponsor)
    {
        parent::__construct('Cosmo/Template/sponsor-block');

        $this->sponsor = $sponsor;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->sponsor->getContentInformation()->getBody();
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        /** @var File $image */
        $image = $this->sponsor->getContentFiles()['media'];

        return $image->getDir().$image->getName();
    }

    /**
     * @return string
     */
    public function getLink()
    {
        /** @var ContentData $link */
        $link = $this->sponsor->getContentData()['link'];

        return $link->getData();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->sponsor->getContentInformation()->getTitle();
    }
}