<?php
namespace Airhead\Cosmo\View;

interface NotificationViewInterface
{
    /**
     * @return string[]
     */
    public function getMessages();

    /**
     * @return string
     */
    public function getType();
}