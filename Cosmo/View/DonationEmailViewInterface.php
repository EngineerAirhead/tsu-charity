<?php
namespace Airhead\Cosmo\View;

interface DonationEmailViewInterface
{
    /**
     * @return string
     */
    public function getBaseUrl();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getSponsors();
}