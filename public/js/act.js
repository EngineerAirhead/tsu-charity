(function() {
    "use strict";

    var streamerPlayer;

    // Init custom functions
    showhideDonate();
    $('.timer').each(count);
    $('.carousel').carousel({interval: 5000, pause: null});

    // Scroll actions
    $(window).scroll(function() {
        if ($(".navbar").offset().top > 100) {
            $(".navbar-fixed-top").addClass("top-nav-collapse");
        } else {
            $(".navbar-fixed-top").removeClass("top-nav-collapse");
        }

        showhideDonate();
    });

    // Resize actions
    $(window).resize(function() {
        showhideDonate();
    });

    // Page scroller
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top-50
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    //Rotate toggle icon
    $('#contact-details').on('hide.bs.collapse', function () {
        $('#contact-toggle').find('i').toggleClass('fa-chevron-down fa-chevron-up', 400);
    });
    $('#contact-details').on('show.bs.collapse', function () {
        $('#contact-toggle').find('i').toggleClass('fa-chevron-down fa-chevron-up', 400);
    });

    //Modernizer touch
    if (Modernizr.touch) {
        // show the close overlay button
        $(".close-overlay").removeClass("hidden");
        // handle the adding of hover class when clicked
        $(".img").click(function(e) {
            if (!$(this).hasClass("hover")) {
                $(this).addClass("hover");
            }
        });
        // handle the closing of the overlay
        $(".close-overlay").click(function(e) {
            e.preventDefault();
            e.stopPropagation();
            if ($(this).closest(".img").hasClass("hover")) {
                $(this).closest(".img").removeClass("hover");
            }
        });
    } else {
        // handle the mouseenter functionality
        $(".img").mouseenter(function() {
            $(this).addClass("hover");
        })
        // handle the mouseleave functionality
            .mouseleave(function() {
                $(this).removeClass("hover");
            });
    }

    // Activate first streamer tab or hide streamer segment
    if ($('.streamer-box').length > 0) {
        streamerPlayer = new Twitch.Player("streamFrame", {
            width: $('#streamFrame').width(),
            height: $('#streamFrame').height()
        });

        var liveStreamer = shuffle($("#stream .streamer-box[data-online='true']")).slice(0, 1);
        if (liveStreamer.length === 0) {
            liveStreamer = $("#stream .streamer-box").first();
        }
        activateStreamer(liveStreamer);

        $('.streamer-box').on('click', function(){
            activateStreamer($(this));
        });
    } else {
        $('#stream').hide();
    }

    // Handle donation amount buttons
    $('.donate-amount').on('click', function() {
        $('.donate-amount').removeClass('active');
        $(this).addClass('active');
        $('input[name="amount"]').val($(this).val());
    });

    // Handle donation form submit
    $('#donationForm').on('submit', function(event){
        event.preventDefault();

        var form = $(this);
        var notifElement = $('#donateFormNotif');

        notifElement.html('');

        var payPalWindow = window.open();
        payPalWindow.document.write("<p>Creating a PayPal transaction, give us some time :)</p>");

        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            dataType: 'json'
        }).done(function(data) {
            if (data.status === 'success') {
                notifElement.html(data.feedback.message);
                payPalWindow.location = data.feedback.redirect_url;
            } else {
                notifElement.html(data.feedback);
            }
        });
    });

    // Handle newsletter subscription
    $('.footer-subscribe').on('submit', function(event){
        event.preventDefault();

        var element = $(this);
        var input = $('input[name="'+$(this).data('input')+'"]');
        var email = $.trim(input.val());

        input.parent().removeClass('has-error');

        if (email !== '') {
            $.ajax({
                method: 'POST',
                url: "newsletter",
                data: {'email': email},
                dataType: 'json'
            }).done(function(data) {
                if (data.status === 'success') {
                    element.fadeOut();
                } else {
                    input.parent().addClass('has-error');
                }

                alert(data.feedback);
            });
        } else {
            input.parent().addClass('has-error');
        }
    });

    // Handle contact form
    $('#contactform').on('submit', function(event){
        event.preventDefault();

        var responseblock = $('#contactResponse');
        responseblock.html('');

        $.ajax({
            method: 'POST',
            url: "contact",
            data: $(this).serialize(),
            dataType: 'json'
        }).done(function(data) {
            responseblock.html(data.feedback);
        });
    });

    // Custom functions
    function count(options) {
        var $this = $(this);
        options = $.extend({}, options || {}, $this.data('countToOptions') || {});
        $this.countTo(options);
        return true;
    }

    function activateStreamer(element) {
        $('.streamer-box').removeClass('active');
        streamerPlayer.setChannel(element.data('streamer'));
        $('#streamChat').attr('src', 'https://www.twitch.tv/'+element.data('streamer')+'/chat');
        element.addClass('active');
    }

    function showhideDonate() {
        if ($(window).width() >= 977 && $(".navbar").offset().top > 100) {
            //$("#donate-homepage").show();
        } else {
            //$("#donate-homepage").hide();
        }
    }

    function shuffle(array) {
        var m = array.length, t, i;
        while (m) {
            i = Math.floor(Math.random() * m--);
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }
        return array;
    }

})(jQuery);