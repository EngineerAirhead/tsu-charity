$(function(){
    function pageLoad(){

        $('.selectpicker').selectpicker({
            format: false
        });

        $("#user-form").parsley({
            errorsContainer: function ( parsleyField ) {
                return parsleyField.$element.parents(".form-group").children("label");
            }
        });

        $('.widget').widgster();
    }

    pageLoad();
});