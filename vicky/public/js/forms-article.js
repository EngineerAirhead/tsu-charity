$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

    input.trigger('fileselect', [numFiles, label]);
});

$(function(){
    function pageLoad(){
        $('.date-picker').datetimepicker();

        $('.selectpicker').selectpicker({
            format: false
        });

        $("#phone, #fax").mask("(999) 999-9999");
        $("#publish-time").mask("99:99");

        //teach select2 to accept data-attributes
        $(".chzn-select").each(function(){
            $(this).select2($(this).data());
        });
        $("#article-tags").select2({
            tags: ['photoshop', 'colors', 'plugins', 'themes', 'bike']
        });

        $(".wysisyg-editor").summernote({
            height: 400,
            callbacks: {
                onImageUpload: function(files) {
                    // Add uploaded image callback
                }
            }
        }).on('summernote.image.upload', function(we, files) {
            sendFile(files[0], we);
        });

        $("#article-form").parsley();

        $('.widget').widgster();
    }

    pageLoad();

    $(':file').on('fileselect', function(event, numFiles, label) {
        $(this).closest('.form-group').find('.file-feedback').val(label);
    });

    function sendFile(file, editor) {
        var uploadUrl = $(editor.target).data('upload-url');
        var data = new FormData();
        data.append("file", file);
        $.ajax({
            data: data,
            type: "POST",
            url: uploadUrl,
            cache: false,
            contentType: false,
            processData: false,
            success: function(result) {
                if (result.status == 'success') {
                    console.log(result.feedback);
                    $(editor.target).summernote('insertImage', result.feedback.url, result.feedback.filename);
                }
            }
        });
    }
});